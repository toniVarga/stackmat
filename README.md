# stackmat

Use this library to read from stackmat timer.
Communication between ESP32 and stackmat is realised over max232 IC.
RS232 signals from stackmat are transformed to UART signals.
Result is UART communication with folowing parameters:
	baud rate: 1200
	data bits: 8
	stop bits: 1
	parity: none
	flow control: none
	
Schematics will be added soon.