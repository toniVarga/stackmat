/*
 * stackmat.h
 *
 *  Created on: 21. lis 2018.
 *      Author: Toni Varga
 */
#include <inttypes.h>

#ifndef __STACKMAT__
#define __STACKMAT__

typedef int bool;
#define true 1
#define false 0

void sm_get_time(char*);

#endif
